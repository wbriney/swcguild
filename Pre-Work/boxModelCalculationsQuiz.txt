


Calculation Assignment
In this exercise, you will apply the equations for calculating heights and widths as discussed in the Box Model lesson.
The sample element you are working with is:
#div1 {
height: 150px;
width: 400px;
margin: 20px;
border: 1px solid red;
padding: 10px;
}
For this object, calculate:
�	Total height
�	Total width
�	Browser calculated height
�	Browser calculated width
Save your answers in a file named boxModelCalculationsQuiz.txt and be sure to show your work as to how you arrived at the answers.
Submit this file to Moodle.
-----------------------------------------------------------------------------------------------------------------------------------------
Total Height 
20+1+10+150+10+1+20=212

Total Width
20+1+10+400+10+1+20=462

Browser Calculated Height
1+10+150+10+1=172

Browser Calculated Width
1+10+400+10+1=422
